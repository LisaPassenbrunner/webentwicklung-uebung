<?php

namespace Database\Seeders;

use App\Models\Person;
use Illuminate\Database\Seeder;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class VaccinationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vaccination = new \App\Models\Vaccination;
        $vaccination->code = '19343948';
        $vaccination->date = new DateTime();
        $vaccination->time = '08:00 - 09:00';
        $vaccination->vaccine = 'Astrazenca';
        $vaccination->description = 'Corana Impfung Ü75';
        $vaccination->max_registrations = '3';
        $vaccination->registrations = '0';
        $vaccination->created_at = date("Y-m-d H:i:s");
        $vaccination->updated_at = date("Y-m-d H:i:s");
        $vaccination->save();

        $place = new \App\Models\Place;

        $place->plz = '4491';
        $place->title = 'Impfstelle Niederneukirchen';
        $place->place = 'Niederneukirchen';
        $place->street = 'Ruprechtshofen';
        $place->number = '33';
        $place->district = 'Oberoesterreich';
        $place->created_at = date("Y-m-d H:i:s");
        $place->updated_at = date("Y-m-d H:i:s");

        $vaccination->place()->save($place);

        $vaccination1 = new \App\Models\Vaccination;
        $vaccination1->code = '3984093';
        $vaccination1->date = new DateTime();
        $vaccination1->time = '08:00 - 17:00';
        $vaccination1->vaccine = 'Biontech';
        $vaccination1->description = 'Corana Impfung Ü75';
        $vaccination1->max_registrations = '15';
        $vaccination1->registrations = '0';
        $vaccination1->created_at = date("Y-m-d H:i:s");
        $vaccination1->updated_at = date("Y-m-d H:i:s");

        $vaccination1->save();

        $place1 = new \App\Models\Place;

        $place1->plz = '4232';
        $place1->title = 'Impfstelle Hagenberg';
        $place1->place = 'Hagenberg';
        $place1->street = 'Softwarepark';
        $place1->number = '23';
        $place1->district = 'Oberoesterreich';
        $place1->created_at = date("Y-m-d H:i:s");
        $place1->updated_at = date("Y-m-d H:i:s");

        $vaccination1->place()->save($place1);
        $vaccination1->save();

        $vaccination2 = new \App\Models\Vaccination;
        $vaccination2->code = '23452345';
        $vaccination2->date = new DateTime();
        $vaccination2->time = '08:00 - 10:00';
        $vaccination2->vaccine = 'Astrazeneca';
        $vaccination2->description = 'Corana Impfung';
        $vaccination2->max_registrations = '4';
        $vaccination2->registrations = '0';

        $vaccination2->created_at = date("Y-m-d H:i:s");
        $vaccination2->updated_at = date("Y-m-d H:i:s");
        $vaccination2->save();

        $place2 = new \App\Models\Place;

        $place2->plz = '4020';
        $place2->title = 'Impfstelle Linz';
        $place2->place = 'Linz';
        $place2->street = 'Linzstraße';
        $place2->number = '10';
        $place2->district = 'Oberoesterreich';
        $place2->created_at = date("Y-m-d H:i:s");
        $place2->updated_at = date("Y-m-d H:i:s");

        $vaccination2->place()->save($place2);
        $vaccination2->save();

        $vaccination3 = new \App\Models\Vaccination;
        $vaccination3->code = '909738392';
        $vaccination3->date = new DateTime();
        $vaccination3->time = '10:00 - 12:00';
        $vaccination3->vaccine = 'Astrazeneca';
        $vaccination3->description = 'Corana Impfung';
        $vaccination3->max_registrations = '4';
        $vaccination3->registrations = '0';

        $vaccination3->created_at = date("Y-m-d H:i:s");
        $vaccination3->updated_at = date("Y-m-d H:i:s");
        $vaccination3->save();

        $vaccination3->place()->save($place2);
        $vaccination3->save();


    }
}
