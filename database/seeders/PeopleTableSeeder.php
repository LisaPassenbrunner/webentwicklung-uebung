<?php

namespace Database\Seeders;

use App\Models\Person;
use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $person1 = new \App\Models\Person;
        $person1->sv_nr = "3121";
        $person1->firstName = "Lisa";
        $person1->lastName = "Passenbrunner";
        $person1->gender = "weiblich";
        $person1->dateOfBirth = "2000-01-07";
        $person1->email = "lisa.passenbrunner@gmx.at";
        $person1->password = "secret";
        $person1->phoneNumber = "06504033328";
        $person1->vaccinated = "0";
        $person1->administrator = "false";
        $person1->password = bcrypt('secret');
        $person1->save();

        $person2 = new Person;
        $person2->sv_nr = "3456";
        $person2->firstName = "Lisa";
        $person2->lastName = "Millinger";
        $person2->gender = "weiblich";
        $person2->dateOfBirth = "2000-08-06";
        $person2->email = "lisa.millinger@gmx.at";
        $person2->password = "secret";
        $person2->phoneNumber = "06504033327";
        $person2->vaccinated = "0";
        $person2->administrator = "false";
        $person2->password = bcrypt('secret1');
        $person2->save();

        $person2 = new Person;
        $person2->sv_nr = "4567";
        $person2->firstName = "Else";
        $person2->lastName = "Passenbrunner";
        $person2->gender = "weiblich";
        $person2->dateOfBirth = "1969-11-11";
        $person2->email = "else.passenbrunner@gmx.at";
        $person2->password = "secret";
        $person2->phoneNumber = "06504055527";
        $person2->vaccinated = "0";
        $person2->administrator = "false";
        $person2->password = bcrypt('secret1');
        $person2->save();

        $person2 = new Person;
        $person2->sv_nr = "5632";
        $person2->firstName = "Admin";
        $person2->lastName = "Admin";
        $person2->gender = "weiblich";
        $person2->dateOfBirth = "1969-11-11";
        $person2->email = "admin.admin@gmx.at";
        $person2->password = "secret";
        $person2->phoneNumber = "06504055527";
        $person2->vaccinated = "0";
        $person2->administrator = "true";
        $person2->password = bcrypt('admin');
        $person2->save();
    }
}


