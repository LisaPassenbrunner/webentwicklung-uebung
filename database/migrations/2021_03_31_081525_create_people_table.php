<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Place;
use App\Models\Vaccination;
use App\Models\Person;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id()->unique();
            $table->string('sv_nr');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('gender');
            $table->date('dateOfBirth');
            $table->string('email')->unique();
            $table->string('phoneNumber');
            $table->string('administrator')->default("false");
            $table->boolean('vaccinated');
            $table->string('vaccination_id')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
