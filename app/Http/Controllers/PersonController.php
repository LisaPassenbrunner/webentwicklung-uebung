<?php

namespace App\Http\Controllers;

use App\Models\Person;
use Illuminate\Http\Request;
use App\Models\Place;
use App\Models\Vaccination;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Psy\Util\Json;

class PersonController extends Controller
{
    /**
     * REST
     */

    public function index(){
        //load all objects
        $people = Person::with(['vaccination'])->get();
        return $people;
    }

    /**
     * Find vaccinations by sv_nr
     */
    public function findBySVNR($sv_nr) : Person{
        $person = Person::where('sv_nr', $sv_nr)
            ->with(['vaccination'])
            ->first();
        return $person;
    }

    /**
     * update Person
     */
    public function update(Request $request, string $sv_nr) : JsonResponse{
        DB::beginTransaction();
        try{
            $person = Person::with(['vaccination'])
                ->where('sv_nr', $sv_nr)->first();
            if($person != null) {
                $request = $this->parseRequest($request);
                if($request->vaccinated == true){
                    $person->vaccinated = 1;
                }
                else $person->vaccinated = 0;
                $person->save();
            }
            DB::commit();
            $person1 = Person::with(['vaccination'])
                ->where('sv_nr', $sv_nr)->first();
            return response()->json($person1, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating person failed: " . $e->getMessage(), 420);
        }

    }

    private function parseRequest(Request $request) : Request {
        // get date and convert it - its in ISO 8601, e.g. "2018-01-01T23:00:00.000Z"
        $date = new \DateTime($request->date);
        $request['date'] = $date;
        return $request;
    }
}
