<?php

namespace App\Http\Controllers;

use App\Models\Person;
use App\Models\Place;
use App\Models\Vaccination;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Psy\Util\Json;


class VaccinationController extends Controller
{
    /**
     * delete Vaccination
     */
    public function delete(string $code): JsonResponse{
        $vaccination = Vaccination::where('code', $code)->first();
        if($vaccination != null){
            $vaccination->delete();
        }
        else
            throw new \Exception("vaccination couldnt´t be deleted - it doesn´t exist");
        return response()->json('vaccination (' .$code. ') successfully deleted', 200);
    }

    /**
     * update Vaccination
     */
    public function update(Request $request, string $code) : JsonResponse{
        DB::beginTransaction();
        try{
            $vaccination = Vaccination::with(['people', 'place'])
                ->where('code', $code)->first();
            if($vaccination != null) {
                $request = $this->parseRequest($request);
                $vaccination->update($request->all());

                $vaccination->place()->delete();

                //save places
                if (isset($request['place']) && is_array($request['place'])) {
                    foreach ($request['place'] as $place) {
                        $newPlace =
                            Place::firstOrNew(['plz' => $place['plz'], 'title' => $place['title'], 'place' => $place['place'],
                                'street' => $place['street'], 'number' => $place['number'], 'district' => $place['district'],]);
                        $vaccination->place()->save($newPlace);
                    }
                }
                $vaccination->save();
            }
            DB::commit();
            $vaccination1 = Vaccination::with(['people', 'place'])
                ->where('code', $code)->first();
            return response()->json($vaccination1, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating vaccination failed: " . $e->getMessage(), 420);
        }
    }

    /**
     * update Vaccination Registration
     */
    public function updateRegistration(Request $request, string $code) : JsonResponse{
        DB::beginTransaction();
        try{
            $vaccination = Vaccination::with(['people', 'place'])
                ->where('code', $code)->first();
            if($vaccination != null) {
                $request = $this->parseRequest($request);

                $ids = [];
                //save people
                if (isset($request['people']) && is_array($request['people'])) {
                    foreach ($request['people'] as $person) {
                        $currentPerson = Person::with(['vaccination'])->where('id', $person['id'])->first();
                        $currentPerson->vaccination_id = $vaccination['code'];
                        $currentPerson->save();
                        $person['vaccination_id'] = $vaccination['code'];
                        array_push($ids, $person['id']);
                    }
                    $vaccination->registrations = count($request['people']);
                }
                $vaccination->people()->sync($ids);
                $vaccination->save();
            }
            DB::commit();
            $vaccination1 = Vaccination::with(['people', 'place'])
                ->where('code', $code)->first();
            return response()->json($vaccination1, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating vaccination registration failed: " . $e->getMessage(), 420);
        }

    }

    public function save(Request $request) : JsonResponse
    {
        //verarbeiten von dateTime
        $request = $this->parseRequest($request);

        DB::beginTransaction();
        try {
            $vaccination = Vaccination::create($request->all());

            //save place
            if (isset($request['place']) && is_array($request['place'])) {
                foreach($request['place'] as $place) {
                    $placeNew = Place::firstOrNew(['title' => $place['title'], 'plz' => $place['plz'], 'place' => $place['place'],
                        'street' => $place['street'], 'number' => $place['number'],
                        'district' => $place['district']]);
                    $vaccination->place()->save($placeNew);
                }
            }
            DB::commit();
            //return http response
            return response()->json($vaccination, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("saving vaccination failed: ". $e->getMessage(), 420);
        }
    }


    private function parseRequest(Request $request) : Request {
        // get date and convert it - its in ISO 8601, e.g. "2018-01-01T23:00:00.000Z"
        $date = new \DateTime($request->date);
        $request['date'] = $date;
        return $request;
    }


    /**
     * REST
     */

    public function index(){
        //load all objects
        $vaccinations = Vaccination::with(['place', 'people'])->get();
        return $vaccinations;
    }

    /**
     * Find vaccinations by id
     */
    public function findById($code) : Vaccination{
        $vaccination = Vaccination::where('code', $code)
            ->with(['people', 'place'])
            ->first();
        return $vaccination;
    }


    /**
     * Check if vaccination exists
     */
    public function checkId($id) {
        $vaccination = Vaccination::where('id', $id)->first();
        return $vaccination != null ? response()->json(true, 200) : response()->json(false, 200);
    }

    /**
     * find vaccination by search term
     */
    public function findBySearchTerm(string $searchTerm){
        $vaccination = Vaccination::with(['place', 'people'])
            ->where('vaccine', 'LIKE', '%' . $searchTerm . '%')
            ->orWhere('description', 'LIKE', '%' . $searchTerm . '%')
            ->orWhereHas('place', function($query) use ($searchTerm){
                $query->where('plz' , 'LIKE', '%' . $searchTerm . '%')
                    ->orWhere('place' , 'LIKE', '%' . $searchTerm . '%')
                    ->orWhere('district' , 'LIKE', '%' . $searchTerm . '%')
                    ->orWhere('street' , 'LIKE', '%' . $searchTerm . '%');
            })->get();
        return $vaccination;
    }
}
