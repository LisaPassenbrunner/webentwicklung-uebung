<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use function Models\HasMany;

class Place extends Model
{
    use HasFactory;

    protected $fillable = [
        'plz', 'title', 'place', 'street', 'number', 'district'
    ];


    public function vaccination() : BelongsTo
    {
        return $this->belongsTo(Vaccination::class);
    }
}
