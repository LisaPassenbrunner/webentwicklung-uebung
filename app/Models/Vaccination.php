<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Vaccination extends Model
{
    /**
     * Attributes that are mass assignable
     */
    use HasFactory;

    protected $fillable = [ 'code', 'date', 'vaccine', 'description', 'time', 'max_registrations', 'registrations'];

    /**
     * vaccination "belongs" to many people
     */
    public function people() : BelongsToMany{
        return $this->belongsToMany(Person::class);
    }

    public function place() : HasMany
    {
        return $this->hasMany(Place::class);
    }
}
