<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/person', function (Request $request) {
    return $request->person();
});

// Vaccination Controller
Route::get('vaccinations', [\App\Http\Controllers\VaccinationController::class,'index']);
Route::get('vaccinations/{id}', [\App\Http\Controllers\VaccinationController::class, 'findById']);
Route::get('vaccinations/checkid/{id}', [\App\Http\Controllers\VaccinationController::class, 'checkId']);
Route::get('vaccinations/search/{searchTerm}', [\App\Http\Controllers\VaccinationController::class, 'findBySearchTerm']);
Route::post('vaccination', [\App\Http\Controllers\VaccinationController::class, 'save']);
Route::put('vaccinations/checkVaccination/{id}/{sv_nr}', [\App\Http\Controllers\VaccinationController::class, 'checkVaccination']);
Route::delete('vaccination/{id}', [\App\Http\Controllers\VaccinationController::class, 'delete']);
Route::put('vaccination/{id}', [\App\Http\Controllers\VaccinationController::class, 'update']);
Route::put('registration/{code}', [\App\Http\Controllers\VaccinationController::class, 'updateRegistration']);

// Person Controller
Route::get('profile/{sv_nr}', [\App\Http\Controllers\PersonController::class, 'findBySVNR']);
Route::get('registrations', [\App\Http\Controllers\PersonController::class,'index']);
Route::get('registrations/{sv_nr}', [\App\Http\Controllers\PersonController::class, 'findBySVNR']);
Route::put('registrations/{sv_nr}', [\App\Http\Controllers\PersonController::class, 'update']);

// Auth Controller
Route::post('auth/login', [\App\Http\Controllers\AuthController::class,'login']);
Route::post('auth/logout', [\App\Http\Controllers\AuthController::class, 'logout']);

// methods with authentication needed
Route::group(['middleware' => ['api', 'auth.jwt']], function(){

});
