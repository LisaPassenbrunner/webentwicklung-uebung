<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>KWM-Corana</title>

    </head>
    <body >
        <ul>
            @foreach ($vaccinations as $vaccination)
                <li>{{$vaccination->id}} {{$vaccination->vaccine}}</li>
            @endforeach
        </ul>
    </body>
</html>
